# Pensum

## Objective
Obtain the graph representation of a structured data for the list in [pensum.json](https://gitlab.com/jmpelaez/reto-pensum/-/raw/main/curso1.json).

A valid entry is a single page web application written in HTML5, CSS3 and plain Javascript (any open source frameworks or libraries may be used). The page must present the graph as SVG interconnected shapes. Connecting lines are optional. The shapes shown here are indicative only. An example of a valid end result is:

![](curso1.png)

## Deadline
The deadline is indicated in the invitation letter.

## License
Each author retains the rights to his/her code, but each entry shall have a OSI approved license in order to be considered for participation.

## Conditions
- The entry shall read and parse the input file as json. The correctness of the program will be determined by using a diferent list that the one provided.
- There must be a clear separation of concern: model, visualization and functionality.

## Results
